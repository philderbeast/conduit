module Main where

import Control.Monad.IO.Class
import Data.Conduit
import qualified Data.Conduit.List as CL

source :: Source IO Int
source = CL.sourceList [1 .. 4]

sink :: Sink String IO ()
sink = awaitForever $ liftIO . putStrLn

conduit :: Conduit Int IO String
conduit = CL.map show

conduitTimes2 :: Conduit Int IO Int
conduitTimes2 = CL.map (*2)

sourceList :: Monad m => [a] -> Source m a
sourceList = mapM_ yield

myAwaitForever :: Monad m => (a -> Conduit a m b) -> Conduit a m b
myAwaitForever f = await >>= maybe (return ()) (\x -> f x >> myAwaitForever f)

main :: IO ()
main = do
    source $$ conduitTimes2 =$= conduit =$ sink